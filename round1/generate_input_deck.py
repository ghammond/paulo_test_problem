import sys
import os

K_m_per_day_to_perm_m2 = 1./(3600.*24.) * 1.e-3 / (1000. * 9.8068)

observation_frequency = 100.

round_ = 'round1'

f_param = open('{}_parameters.txt'.format(round_),'w')

def compile_keywords(length=1000.,height=80.,
                     xK=0.6,
                     dx=10.,dz=10.,
                     hwest=10.,heast=10.,hinit=10., # m
                     Kwest=1.,Keast=0.01,           # m/d
                     recharge=365.                  # mm/y
                     ):
    nx = int(length/dx+1.e-10)
    nz = int(height/dz+1.e-10)
    keyword_list = []
    keyword_list.append(('PARAM_NX','{}'.format(nx)))
    keyword_list.append(('PARAM_NZ','{}'.format(nz)))
    keyword_list.append(('PARAM_LENGTH','{}'.format(length)))
    keyword_list.append(('PARAM_HEIGHT','{}'.format(height)))
    keyword_list.append(('PARAM_XK','{}'.format(xK*length)))
    keyword_list.append(('PARAM_HINIT','{}'.format(hinit)))
    keyword_list.append(('PARAM_HWEST','{}'.format(hwest)))
    keyword_list.append(('PARAM_HEAST','{}'.format(heast)))
    keyword_list.append(('PARAM_PERM_WEST','{:.8e}'.format(Kwest*K_m_per_day_to_perm_m2)))
    keyword_list.append(('PARAM_PERM_EAST','{:.8e}'.format(Keast*K_m_per_day_to_perm_m2)))
    keyword_list.append(('PARAM_RECHARGE_MM_PER_Y','{:.8e}'.format(recharge)))
    return keyword_list
    
def write_observation(f,dist):
    d = int(dist)
    region_name = 'observation_{}m'.format(d)
    f.write('REGION {}\n'.format(region_name))
    f.write('  COORDINATE {} 0.5 0.\n'.format(d))
    f.write('END\n\n')
    f.write('OBSERVATION obs_{}m\n'.format(d))
    f.write('  REGION {}\n'.format(region_name))
    f.write('  AT_CELL_CENTER\n')
    f.write('END\n\n')

def write_input_deck(keyword_list,filename):
    num_keywords = len(keyword_list)
    length = 0.
    for i in range(num_keywords):
        if keyword_list[i][0] == 'PARAM_LENGTH':
            length = float(keyword_list[i][1])
            break
    if length < 1.:
        sys.exit('Lenght not set properly.')
    f = open(filename,'w')

    template_filename = 'K12_input_deck.template'
    template = open(template_filename,'r')

    for line in template:
        if len(line.strip()) > 0:
            word = line.strip().split()[0]
            if word.startswith('END_SUBSURFACE'):
                f.write('#=========================== observation points '
                        '===============================\n')
                dist = observation_frequency
                while(dist < length):
                    write_observation(f,dist)
                    dist += observation_frequency
            else:
                for i in range(num_keywords):
                    line = line.replace(keyword_list[i][0],keyword_list[i][1])
        f.write(line)
    f.close()

    f_param.write('{}\n'.format(filename))
    f_param.write('Parameters:\n')
    for entry in keyword_list:
        f_param.write('  {} : {}\n'.format(entry[0],entry[1]))
    f_param.write('\n\n')

keywords = compile_keywords(dx=1.,dz=1,hwest=10.,heast=12.,recharge=365.)
write_input_deck(keywords,'./365/pflotran.in')

keywords = compile_keywords(dx=1.,dz=1,hwest=10.,heast=12.,recharge=182.5)
write_input_deck(keywords,'./182.5/pflotran.in')

keywords = compile_keywords(dx=1.,dz=1,hwest=10.,heast=12.,recharge=36.5)
write_input_deck(keywords,'./36.5/pflotran.in')

keywords = compile_keywords(length=100.,dx=1.,dz=1,hwest=10.,heast=12.,recharge=365.)
write_input_deck(keywords,'./365_100/pflotran.in')

keywords = compile_keywords(length=100.,dx=1.,dz=1,hwest=10.,heast=12.,recharge=182.5)
write_input_deck(keywords,'./182.5_100/pflotran.in')

keywords = compile_keywords(length=100.,dx=1.,dz=1,hwest=10.,heast=12.,recharge=36.5)
write_input_deck(keywords,'./36.5_100/pflotran.in')

f_param.close()
