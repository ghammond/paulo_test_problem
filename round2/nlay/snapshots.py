import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')
sys.path.append('/scratch/debug/paulo')
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import h5py
import pflotran as pft
import dupuit as dp

filename_roots = []
filename_roots.append('./80/pflotran')
filename_roots.append('./40/pflotran')
filename_roots.append('./20/pflotran')
filename_roots.append('./10/pflotran')
filename_roots.append('./1/pflotran')

legend_entry = []
legend_entry.append('80')
legend_entry.append('40')
legend_entry.append('20')
legend_entry.append('10')
legend_entry.append('1')
legend_title = 'Number of layers [-]'

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Head vs Distance",fontsize=16)
plt.xlabel('X [m]')
plt.ylabel('Head [m]')

#plt.xlim(0.,1.)
#plt.ylim(0.,20.)
plt.grid(True)

patm = 101325.

#if True:
#    root = 'pflotran_1m_22m'
ifile = 0
for root in filename_roots:
    filename = root + '.h5'
    print(filename)
    try:
        h5file = h5py.File(filename,'r')
    except:
        print('File {} not found.'.format(filename))
    z1 = h5file['Coordinates/Z [m]']
    elevation = 0.5*(z1[0]+z1[1])
    print('{} - elevation: {}'.format(filename,elevation))
    x1 = h5file['Coordinates/X [m]']
    x = np.zeros(len(x1)-1,'float')
    for i in range(len(x1)-1):
        x[i] = 0.5*(x1[i]+x1[i+1])
    y1 = h5file['/Time:  1.00000E+06 y/Liquid_Pressure [Pa]']
    y = np.zeros(len(x1)-1,'float')
    for i in range(len(x1)-1):
        y[i] = (y1[i][0][0]-patm)/(1000.*9.81) + elevation
    h5file.close()
    plt.plot(x,y,label=legend_entry[ifile])
    ifile += 1

# add analytical solution
recharge = 1.e-3 # m/d
hwest = 5.
heast = 10.
length = 1000.
xK = 0.6
Kwest = 1.       # m/d
Keast = 0.01     # m/d
x,y = dp.plot_dupuit(recharge,hwest,heast,length,xK,Kwest,Keast,1000)
plt.plot(x,y,ls='--',label='Dupuit')
    
plt.legend(loc=2,title=legend_title)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
#      plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.gca().get_legend().get_frame().set_fill(True)
plt.gca().get_legend().draw_frame(True)
#        plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

plt.show()
