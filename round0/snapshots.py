import sys
import os
try:
  pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
  print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
  sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math
import h5py
import pflotran as pft

filename_roots = []
filename_roots.append('pflotran_1dm_0m')
filename_roots.append('pflotran_1dm_11m')
filename_roots.append('pflotran_1dm_22m')
filename_roots.append('pflotran_1m_0m')
filename_roots.append('pflotran_1m_11m')
filename_roots.append('pflotran_1m_22m')

elevations = []
elevations.append(0.05)
elevations.append(0.05)
elevations.append(0.05)
elevations.append(0.5)
elevations.append(0.5)
elevations.append(0.5)

f = plt.figure(figsize=(6,6))
plt.subplot(1,1,1)
f.suptitle("Head vs Distance",fontsize=16)
plt.xlabel('X [m]')
plt.ylabel('Head [m]')

#plt.xlim(0.,1.)
#plt.ylim(0.,20.)
#plt.grid(True)

patm = 101325.

#if True:
#    root = 'pflotran_1m_22m'
ifile = 0
for root in filename_roots:
    filename = root + '.h5'
    print(filename)
    elevation = elevations[ifile]
    try:
        h5file = h5py.File(filename,'r')
    except:
        print('File {} not found.'.format(filename))
    x1 = h5file['Coordinates/X [m]']
    x = np.zeros(len(x1)-1,'float')
    for i in range(len(x1)-1):
        x[i] = 0.5*(x1[i]+x1[i+1])
    y1 = h5file['/Time:  1.00000E+06 y/Liquid_Pressure [Pa]']
    y = np.zeros(len(x1)-1,'float')
    for i in range(len(x1)-1):
        y[i] = (y1[i][0][0]-patm)/(1000.*9.81) + elevation
    h5file.close()
    plt.plot(x,y,label=root)
    ifile += 1
    
plt.legend(loc=2,title='Head [m]')
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
#      plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.gca().get_legend().get_frame().set_fill(False)
plt.gca().get_legend().draw_frame(False)
#        plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

f.subplots_adjust(hspace=0.2,wspace=0.2,
                  bottom=.12,top=.9,
                  left=.12,right=.9)

plt.show()